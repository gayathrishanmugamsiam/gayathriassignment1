import React,{ useEffect, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
  FlatList,
  TextInput
} from 'react-native';
import Swiper from 'react-native-swiper';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import EntypoIcons from 'react-native-vector-icons';
 

const App = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const getService = async () => {
     try {
      const response = await fetch('https://reqres.in/api/users?page=1');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }
  useEffect(() => {
    getService();
  }, []);
  return (
    
    <View style={styles.container}>
      
      <View style={{position: "relative",backgroundColor:"orange",flexDirection:'row' ,display:"flex"}}>
      <Text
          style={{
             fontSize: 18,
            fontWeight: 'bold',
            color: '#333',
            marginTop:30,
          }}>
         Florida,Usa </Text>
      </View>

      <View style={{position: "relative"}}>
         <TextInput  style={styles.input} placeholder="Search"/>
         <MaterialIcons style={{position:"absolute", marginTop:15 ,marginLeft:15}} name="search" size={35} color="#FF6347" />
      </View>
      
      <View style={styles.sliderContainer}>
     
        <Swiper
          horizontal={true}
          height={200}
          activeDotColor="#FF6347">
          <View style={styles.slide}>
            <Image
              source={require('./assets/Service1.png')}
              resizeMode="cover"
              style={styles.sliderImage}
            />
          </View>
          <View style={styles.slide}>
            <Image
              source={require('./assets/Service.png')}
              resizeMode="cover"
              style={styles.sliderImage}
            />
          </View>
          <View style={styles.slide}>
            <Image
              source={require('./assets/Flat2.png')}
              resizeMode="cover"
              style={styles.sliderImage}
            />
          </View>
        </Swiper>
      </View>
<View style={{flexDirection:'row',display:"flex",justifyContent:'space-between',padding:20}}>
      <Text
          style={{
             fontSize: 18,
            fontWeight: 'bold',
            color: '#333',
            marginTop:40

          }}>
         
         
         Top Service </Text>
         <Text
          style={{
             fontSize: 18,
            fontWeight: 'bold',
            color: '#333',
            marginTop:40

          }}>
         
         
      viewMore </Text>
         </View>
      <View style={styles.categoryContainer}>
        <TouchableOpacity
          style={styles.categoryBtn}>
          <View style={styles.categoryIcon}>
            <MaterialIcons name="cleaning-services" size={35} color="#FF6347" />
          </View>
          <Text style={styles.categoryBtnTxt}>Beauty</Text> 
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.categoryBtn}>
          <View style={styles.categoryIcon}>
            <MaterialIcons name="cleaning-services" size={35} color="#FF6347" />
            </View>
          <Text style={styles.categoryBtnTxt}>Cleaning</Text> 
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.categoryBtn}>
          <View style={styles.categoryIcon}>
            <MaterialIcons name="electric-car" size={35} color="#FF6347" />
          </View>
          <Text style={styles.categoryBtnTxt}>Car Service</Text> 
        </TouchableOpacity>
        </View>


        <View style={{ flex: 1, padding: 24 }}>
      {isLoading ? <ActivityIndicator/> : (
        <FlatList
          data={data.data}
          renderItem={({ item }) => (
         <View
         style={{flexDirection:'row',marginVertical:5
      
         }}>
            <Image 
             style={styles.image}
             source={{uri:item.avatar}}
            resizeMode='contain'
        />
              <Text style={{padding:40}}>{item.first_name}, {item.last_name} </Text>
              </View>
          )}
          

        />
      )}
    
    </View>
       </View>
  );
  };





const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
    paddingLeft:50
  },
  container: {
    flex: 1,
  },
  sliderContainer: {
    height: 200,
    width: '90%',
    marginTop: 10,
    justifyContent: 'center',
    alignSelf: 'center',
    borderRadius: 8,
  },

  wrapper: {},

  slide: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent',
    borderRadius: 8,
  },
  sliderImage: {
    height: '100%',
    width: '100%',
    alignSelf: 'center',
    borderRadius: 8,
  },

  categoryContainer: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 25,
    marginBottom: 10,
  },
  categoryBtn: {
    flex: 1,
    width: '30%',
    marginHorizontal: 0,
    alignSelf: 'center',
  },
  MaterialIcon: {
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    width: 70,
    height: 70,
    backgroundColor: '#fdeae7',
    borderRadius: 50,
  },
  categoryBtnTxt: {
    alignSelf: 'center',
   // marginTop: 5,
    color: '#de4f35',
  },

  categoryContainer: {
    flexDirection: 'row',
    width: '90%',
    alignSelf: 'center',
    marginTop: 15,
    marginBottom: 5,
  },
  categoryBtn: {
    flex: 1,
    width: '30%',
    marginHorizontal: 0,
    alignSelf: 'center',
  },
  categoryIcon: {
    borderWidth: 0,
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'center',
    width: 70,
    height: 70,
    backgroundColor: '#fdeae7',
    borderRadius: 50,
  },
  categoryBtnTxt: {
    alignSelf: 'center',
    marginTop: 5,
    color: '#de4f35',
  },
  image: {
    marginTop: 5,
    height: 90,
    width: 90,
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#ffffff',
    borderRadius:10

  }


});


export default App;